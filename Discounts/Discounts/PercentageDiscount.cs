﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discounts
{
    class PercentageDiscount : DiscountStrategy
    {
        private double percentage;

        public PercentageDiscount(double percentage)
        {
            this.percentage = percentage;
        }
        public override double ApplyDiscount(double originalPrice)
        {
            return originalPrice * (1 - percentage / 100);
        }
    }
}
