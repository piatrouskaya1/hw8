﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discounts
{
    public abstract class DiscountStrategy
    {
        public abstract double ApplyDiscount(double originalPrice);
    }
}