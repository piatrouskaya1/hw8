﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discounts
{
    class PaymentProcessor
    {
        public IPaymentStrategy paymentStrategy;
        public ILogger logger;

        public PaymentProcessor(IPaymentStrategy paymentStrategy, ILogger logger)
        {
            this.paymentStrategy = paymentStrategy;
            this.logger = logger;
        }

        public void ProcessPayment(decimal amount) 
        {
            paymentStrategy.ProcessPayment(amount);
            logger.Log($"Payment was done: {amount:C}");
        }
    }
}