﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discounts
{
    class FixedDiscount : DiscountStrategy
    {
        private double fixedAmount;
        public FixedDiscount(double fixedAmount)
        {
            this.fixedAmount = fixedAmount;
        }
        public override double ApplyDiscount(double originalPrice)
        {
            return originalPrice - fixedAmount;
        }
    }
}