﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Discounts
{
    class FileLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine($"Log to the file: {message}");
        }
    }
}