﻿using Discounts;
using System;
class Program
{
    static void Main()
    {
        Product[] products = new Product[]
        {
            new Product("Item 1", 100),
            new Product("Item 2", 50),
            new Product("Item 3", 200)
        };
        ShoppingCart cart = new ShoppingCart(products);
        DiscountStrategy percentageDiscount = new PercentageDiscount(10); 
        DiscountStrategy fixedDiscount = new FixedDiscount(30); 

        cart.SetDiscountStrategy(percentageDiscount);

        double totalWithDiscount = cart.CalculateTotal();
        Console.WriteLine($"General amount with discount: {totalWithDiscount}");

        cart.SetDiscountStrategy(fixedDiscount);

        double totalWithFixedDiscount = cart.CalculateTotal();
        Console.WriteLine($"General amount with fixed discount: {totalWithFixedDiscount}");

        IPaymentStrategy cardPaymentStrategy = new CardPaymentStrategy();
        IPaymentStrategy applePaymentStrategy = new CardPaymentStrategy();

        ILogger consoleLogger = new ConsoleLogger();
        ILogger fileLogger = new FileLogger();

        PaymentProcessor paymentProcessor1 = new PaymentProcessor(cardPaymentStrategy, consoleLogger);
        PaymentProcessor paymentProcessor2 = new PaymentProcessor(applePaymentStrategy, fileLogger);

        paymentProcessor1.ProcessPayment(100);
        paymentProcessor2.ProcessPayment(500);
    }
}