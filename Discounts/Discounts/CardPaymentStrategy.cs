﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Discounts
{
    class CardPaymentStrategy : IPaymentStrategy
    {
        public void ProcessPayment(decimal amount) 
        {
            Console.WriteLine("Payment by card");
        }
    }
}